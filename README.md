acenhauer-repo
=================

Master git of Acenhauer Kodi/XMBC addon repository

Home of Acenhauer and Vanilla Ace addons.

For more details, feature requests and bug reports, see addon specific pages in following url's:

  * https://bitbucket.org/guillemhs/plugin.video.acenhauer
  * https://bitbucket.org/guillemhs/plugin.video.vanillaace

